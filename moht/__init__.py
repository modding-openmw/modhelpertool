VERSION = '1.1.0'
TES3CMD = {
    'win32': {37: 'tes3cmd-0.37v.exe',
              40: 'tes3cmd-0.40-pre_rel2.exe'},
    'linux': {37: 'tes3cmd-0.37w',
              40: 'tes3cmd-0.40-pre_rel2'},
    'darwin': {37: 'tes3cmd-0.37w',
               40: 'tes3cmd-0.40-pre_rel2'},
}
OMWCMD = {
    'win32': 'omwcmd-0.2.1.exe',
    'linux': 'omwcmd-0.2.1',
    'darwin': 'omwcmd-0.2.1',
}
